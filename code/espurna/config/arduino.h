//--------------------------------------------------------------------------------
// These settings are normally provided by PlatformIO
// Uncomment the appropiate line(s) to build from the Arduino IDE
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
// Hardware
//--------------------------------------------------------------------------------

//#define NODEMCU_LOLIN
//#define WEMOS_D1_MINI_RELAYSHIELD
//#define TINKERMAN_ESPURNA_H
//#define ITEAD_SONOFF_BASIC
//#define ITEAD_SONOFF_RF
//#define ITEAD_SONOFF_TH
//#define ITEAD_SONOFF_SV
//#define ITEAD_SLAMPHER
//#define ITEAD_S20
//#define ITEAD_SONOFF_TOUCH
//#define ITEAD_SONOFF_POW
//#define ITEAD_SONOFF_DUAL
//#define ITEAD_SONOFF_4CH
//#define ITEAD_SONOFF_4CH_PRO
//#define ITEAD_1CH_INCHING
//#define ITEAD_MOTOR
//#define ITEAD_SONOFF_BNSZ01
//#define ITEAD_SONOFF_RFBRIDGE
//#define ITEAD_SONOFF_B1
//#define ITEAD_SONOFF_LED
//#define ITEAD_SONOFF_T1_1CH
//#define ITEAD_SONOFF_T1_2CH
//#define ITEAD_SONOFF_T1_3CH
//#define ELECTRODRAGON_WIFI_IOT
//#define WORKCHOICE_ECOPLUG
//#define AITHINKER_AI_LIGHT
//#define MAGICHOME_LED_CONTROLLER
//#define HUACANXING_H801
//#define JANGOE_WIFI_RELAY_NC
//#define JANGOE_WIFI_RELAY_NO
//#define JORGEGARCIA_WIFI_RELAYS
//#define OPENENERGYMONITOR_MQTT_RELAY

//--------------------------------------------------------------------------------
// Features (values below are non-default values)
//--------------------------------------------------------------------------------

//#define ALEXA_SUPPORT          0
//#define ANALOG_SUPPORT         1
//#define DEBUG_SERIAL_SUPPORT   0
//#define DEBUG_UDP_SUPPORT      1
//#define DHT_SUPPORT            1
//#define DOMOTICZ_SUPPORT       0
//#define DS18B20_SUPPORT        1
//#define EMON_SUPPORT           1
//#define I2C_SUPPORT            1
//#define INFLUXDB_SUPPORT       0
//#define MDNS_SUPPORT           0
//#define NOFUSS_SUPPORT         1
//#define NTP_SUPPORT            0
//#define RF_SUPPORT             1
//#define SPIFFS_SUPPORT         1
//#define TERMINAL_SUPPORT       0
//#define WEB_SUPPORT            0
